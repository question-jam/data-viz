# Data visualization component library

## Usage
Transpile (JSX support)
```
npx babel src --out-dir dist
```

## Local development
#### Imported components
Delete `node_modules` directory, if any exists in data-viz project

Install regular dependencies (no peer or dev dependencies)
```
npm install --production
```
Start watcher to transpile sources automatically as new changes are saved
```
npm run build-watch
```
Make the package available for linking on your machine
```
npm link
```
Install the package in the consuming app
```
npm install https://gitlab.com/qvestio/data-viz.git
```
Link the package
```
npm link data-viz
```
Ensure the consuming application has a webpack configuration that sets `resolve.symlinks = true` Or if importing to a node app that the  `--preserve-symlinks` flag is set. This is required to ensure peer dependencies are resolved in the consuming app rather than with duplicates. Some packages require a single version per runtime (e.g. React). These measures are only needed becuase we are using `npm link` to facilitate local development. Data-viz can be imported cleanly otherwise.
