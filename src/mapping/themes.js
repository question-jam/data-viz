import _ from 'lodash'


// Sort themes in with alternating size to avoid overlapping labels caused by many small themes being close together
export function sortForOptimalDisplay(themes) {
  const [small, large]  = _.chain(themes)
    .sortBy(theme => theme.questionIds.length) // Order by question count
    .chunk(Math.ceil(themes.length / 2))       // Separate into 2 chunks (small and large respectively)
    .value()
  return _.chain(_.zip(small, large))          // Zip (interleave) the chunks into each other
    .flatten()                                 // Flatten nested arrays caused by zip operation
    .filter(t => t)                            // Remove undefined items introduced by un-even zip operation
    .value()
}


export default {
  sortForOptimalDisplay
}
