import sortBy from 'lodash/sortBy'


function groupToBar(group, colorScale) {
  const { groupId, name, participationRate } = group
  return {
    id: groupId,
    value: participationRate,
    label: name,
    fill: colorScale(groupId),
  }
}

function ratePercentage(subset, total) {
  return (total === 0)
    ? null
    : Math.round(subset / total * 10000) / 100
}

export function determineActivityType(participation) {
  return participation
    .map(({ value }) => {
      if (value < 30) return 'LOW'
      else if (value < 60) return 'OK'
      return 'HIGH'
    })
    .reduce((acc, type) => {
      if (acc.isDiverse) return acc
      if (acc.type !== type && acc.type) return { type, isDiverse: true }
      return { type, isDiverse: false }
    }, { type: null, isDiverse: false })
}

// TODO: Add memoization when colors have been decoupled
export function formatData(participation, theme, colorScale) {
  const sortedParticipation = participation
    .map(group => ({
      ...group,
      participationRate: ratePercentage(group.participantActiveCount, group.participantCount),
      // make sure we are not running out of colors
      fill: colorScale(group.groupId),
    }))
    .sort((groupA, groupB) => groupA.participationRate - groupB.participationRate)

  const count = sortedParticipation
    .reduce((acc, { participantCount }) => acc + participantCount, 0)
  const activeCount = sortedParticipation
    .reduce((acc, { participantActiveCount }) => acc + participantActiveCount, 0)
  const sum = {
    groupId: 'ffffffff-ffff-ffff-ffff-ffffffffffff',
    name: 'All',
    participantCount: count,
    participantActiveCount: activeCount,
    participationRate: ratePercentage(activeCount, count),
    fill: theme.main.primary,
  }

  let formattedParticipation = sortedParticipation.map(g => groupToBar(g, colorScale))
  formattedParticipation = sortBy(formattedParticipation, p => p.value)
  const { type, isDiverse } = determineActivityType(formattedParticipation)

  return {
    type,
    isDiverse,
    sortedParticipation,
    sum,
    formattedSum: [groupToBar(sum, () => theme.main.primary)],
    lowest: formattedParticipation[0],
    highest: formattedParticipation.slice(-1)[0]
  }
}

export const createBarInput = participation => participation
  .map(({ groupId, name, participationRate, fill }) => ({
    id: groupId,
    value: participationRate,
    label: name,
    fill,
  }))

export default {
  determineActivityType,
  formatData,
  createBarInput
}