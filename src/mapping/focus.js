

function isFocus(direction1, direction2) {
  if (!direction1 || !direction2 ) return false
  return (direction1.value > 0) && (direction2.value > 0) && (direction1.value + direction2.value > 0.6)
}

function determineDirection(focuses) {
  const { north, east, south, west } = focuses
  if(east && north && west) {
    return { direction: 'north', focusCount: 2 }
  } else if(north && east && south) {
    return { direction: 'east', focusCount: 2 }
  } else if(east && south && west) {
    return { direction: 'south', focusCount: 2 }
  } else if(south && west && north) {
    return { direction: 'west', focusCount: 2 }
  } else if(north && east) {
    return { direction: 'northEast', focusCount: 1 }
  } else if(south && east) {
    return { direction: 'southEast', focusCount: 1 }
  } else if(south && west) {
    return { direction: 'southWest', focusCount: 1 }
  } else if(north && west) {
    return { direction: 'northWest', focusCount: 1 }
  }
  return { focusCount: 0, direction: null }
}

export function formatData(questionProfile) {
  const { why, what, who, how } = questionProfile.count
  const total = (why + what + who + how)
  const north = { value: ((why / total) || 0), label: 'Why', key: 'why'}
  const east = { value: ((what / total) || 0), label: 'What', key: 'what' }
  const south = { value: ((how / total) || 0), label: 'How', key: 'how' }
  const west = { value: ((who / total) || 0), label: 'Who', key: 'who'}

  let focuses = {}
  let blindspots = {}
  if (isFocus(north, east)) {
    focuses = { ...focuses, north: true, east: true }
    blindspots = { ...blindspots, south: true, west: true }
  }
  if (isFocus(north, west)) {
    focuses = { ...focuses, north: true, west: true }
    blindspots = { ...blindspots, south: true, east: true }
  }
  if (isFocus(south, east)) {
    focuses = { ...focuses, south: true, east: true }
    blindspots = { ...blindspots, north: true, west: true }
  }
  if (isFocus(south, west)) {
    focuses = {...focuses, south: true, west: true}
    blindspots = {...blindspots, north: true, east: true}
  }

  const { direction, focusCount } = determineDirection(focuses)
  const hasFocus = (focusCount > 0)

  return {
    data: { north, east, south, west },
    focuses,
    blindspots,
    direction,
    focusCount,
    hasFocus
  }
}

export default {
  formatData,
}