import React from 'react'
import PropTypes from 'prop-types'
import { defineMessages, injectIntl } from 'react-intl'
import { withTheme } from 'styled-components'
import Axis from './Axis'
import AxisLabel from './AxisLabel'


const messages = defineMessages({
  north: { defaultMessage: 'Why', id: 'PolarChart.axisLabel.north' },
  east: { defaultMessage: 'What', id: 'PolarChart.axisLabel.east' },
  south: { defaultMessage: 'How', id: 'PolarChart.axisLabel.south' },
  west: { defaultMessage: 'Who When Where', id: 'PolarChart.axisLabel.west' }
})

const PolarAxes = ({ scales, inverted, show, outerLabels, intl }) => {
  const axes = []
  const labels = []

  // North vertical-axis
  if (show.north) {
    const xScale = show.east ? scales.east : scales.west
    const label = intl.formatMessage(messages.north)
    axes.push(<Axis xScale={xScale} yScale={scales.north} inverted={inverted} />)
    labels.push(<AxisLabel xScale={xScale} yScale={scales.north} label={label} outer={outerLabels} />)
  }
  // East horizontal-axis
  if (show.east) {
    const yScale = show.north ? scales.north : scales.south
    const label = intl.formatMessage(messages.east)
    axes.push(<Axis xScale={scales.east} yScale={yScale} horizontal inverted={inverted} />)
    labels.push(<AxisLabel xScale={scales.east} yScale={yScale} label={label} horizontal outer={outerLabels} />)
  }
  // South vertical-axis
  if (show.south) {
    const xScale = show.west ? scales.west : scales.east
    const label = intl.formatMessage(messages.south)
    axes.push(<Axis xScale={xScale} yScale={scales.south} inverted={inverted} />)
    labels.push(<AxisLabel xScale={xScale} yScale={scales.south} label={label} outer={outerLabels} />)
  }
  // West horizontal-axis
  if (show.west) {
    const yScale = show.south ? scales.south : scales.north
    const label = intl.formatMessage(messages.west)
    axes.push(<Axis xScale={scales.west} yScale={yScale} horizontal inverted={inverted} />)
    labels.push(<AxisLabel xScale={scales.west} yScale={yScale} label={label} horizontal outer={outerLabels} />)
  }

  return (
    <g>
      <g>{axes}</g>
      <g>{labels}</g>
    </g>
  )
}

PolarAxes.propTypes = {
  scales: PropTypes.shape({
    north: PropTypes.func.isRequired,
    east: PropTypes.func.isRequired,
    south: PropTypes.func.isRequired,
    west: PropTypes.func.isRequired,
  }).isRequired,
  show: PropTypes.shape({
    north: PropTypes.bool,
    east: PropTypes.bool,
    south: PropTypes.bool,
    west: PropTypes.bool
  }),
  outerLabels: PropTypes.bool,
  inverted: PropTypes.bool
}

PolarAxes.defaultProps = {
  show: {
    north: true,
    east: true,
    south: true,
    west: true
  },
  outerLabels: false,
  inverted: false
}

export default injectIntl(withTheme(PolarAxes))
