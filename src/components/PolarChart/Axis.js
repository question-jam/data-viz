import React from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'
import * as d3 from 'd3'


function scaleMin(scale) {
  return scale(d3.min(scale.domain()))
}

function scaleMax(scale) {
  return scale(d3.max(scale.domain()))
}

const Axis = ({ theme, inverted, xScale, yScale, horizontal }) => {
  const origin = {
    x: scaleMin(xScale),
    y: scaleMin(yScale)
  }
  const end = {
    x: horizontal ? scaleMax(xScale) : scaleMin(xScale),
    y: horizontal ? scaleMin(yScale) : scaleMax(yScale)
  }

  const mainPath = `M${origin.x},${origin.y} L${end.x},${end.y}`

  const tipWidth = 4
  const tipDeltaX = horizontal ? 0 : tipWidth
  const tipDeltaY = horizontal ? tipWidth : 0
  const tipEdge1 = {
    x: (end.x + tipDeltaX),
    y: (end.y + tipDeltaY)
  }
  const tipEdge2 = {
    x: (end.x - tipDeltaX),
    y: (end.y - tipDeltaY)
  }
  const tip1Path = `M${end.x},${end.y} L${tipEdge1.x},${tipEdge1.y}`
  const tip2Path = `M${end.x},${end.y} L${tipEdge2.x},${tipEdge2.y}`

  const color = inverted ? theme.inverted.one[1] : theme.one[0]

  return (
    <g>
      <path d={mainPath} stroke={color} strokeOpacity="0.4" strokeWidth="2" />
      <path d={tip1Path} stroke={color} strokeOpacity="0.4" strokeWidth="2" />
      <path d={tip2Path} stroke={color} strokeOpacity="0.4" strokeWidth="2" />
    </g>
  )
}

Axis.propTypes = {
  xScale: PropTypes.func.isRequired,
  yScale: PropTypes.func.isRequired,
  horizontal: PropTypes.bool
}

Axis.defaultProps = {
  horizontal: false
}

export default withTheme(Axis)
