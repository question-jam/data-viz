import React from 'react'
import PropTypes from 'prop-types'
import { defineMessages, injectIntl } from 'react-intl'
import { withTheme } from 'styled-components'


const messages = defineMessages({
  northEast: { defaultMessage: 'Purpose', id: 'PolarChart.quadrantLabel.northEast' },
  southEast: { defaultMessage: 'Result', id: 'PolarChart.quadrantLabel.southEast' },
  southWest: { defaultMessage: 'Process', id: 'PolarChart.quadrantLabel.southWest' },
  northWest: { defaultMessage: 'Experience', id: 'PolarChart.quadrantLabel.northWest' }
})

const QuadrantLabels = ({ theme, bounds, show, hidden, intl }) => {
  if (hidden) return null

  const { north, east, south, west } = show
  const padding = 20
  const font = {
    fontFamily: theme.font2,
    fontSize: 18,
    fontWeight: '300'
  }
  const bgStroke = {
    strokeWidth: '10px',  // This allows the text to be read clearly over darker/hatched background
    stroke: theme.tertiary
  }

  let texts = []
  if (north && east) {
    const position = {
      x: (bounds.x.max - padding),
      y: (bounds.y.min + padding),
      textAnchor: 'end',
      alignmentBaseline: 'hanging',
    }
    const content = intl.formatMessage(messages.northEast)
    texts.push(<text {...bgStroke} {...font} {...position}>{content}</text>)
    texts.push(<text {...font} {...position}>{content}</text>)
  }
  if (south && east) {
    const position = {
      x: (bounds.x.max - padding),
      y: (bounds.y.max - padding),
      textAnchor: 'end',
      alignmentBaseline: 'baseline',
    }
    const content = intl.formatMessage(messages.southEast)
    texts.push(<text {...bgStroke} {...font} {...position}>{content}</text>)
    texts.push(<text {...font} {...position}>{content}</text>)
  }
  if (south && west) {
    const position = {
      x: (bounds.x.min + padding),
      y: (bounds.y.max - padding),
      textAnchor: 'start',
      alignmentBaseline: 'baseline',
    }
    const content = intl.formatMessage(messages.southWest)
    texts.push(<text {...bgStroke} {...font} {...position}>{content}</text>)
    texts.push(<text {...font} {...position}>{content}</text>)
  }
  if (north && west) {
    const position = {
      x: (bounds.x.min + padding),
      y: (bounds.y.min + padding),
      textAnchor: 'start',
      alignmentBaseline: 'hanging',
    }
    const content = intl.formatMessage(messages.northWest)
    texts.push(<text {...bgStroke} {...font} {...position}>{content}</text>)
    texts.push(<text {...font} {...position}>{content}</text>)
  }

  return (
    <g>{texts}</g>
  )
}

QuadrantLabels.propTypes = {
  bounds: PropTypes.shape({
    x: PropTypes.shape({
      min: PropTypes.number.isRequired,
      max: PropTypes.number.isRequired
    }),
    y: PropTypes.shape({
      min: PropTypes.number.isRequired,
      max: PropTypes.number.isRequired
    })
  }).isRequired,
  show: PropTypes.shape({
    north: PropTypes.bool,
    east: PropTypes.bool,
    south: PropTypes.bool,
    west: PropTypes.bool
  }),
  hidden: PropTypes.bool
}

QuadrantLabels.defaultProps = {
  show: {
    north: true,
    east: true,
    south: true,
    west: true
  },
  hidden: false
}

export default injectIntl(withTheme(QuadrantLabels))
