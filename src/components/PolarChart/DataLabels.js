import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'
import toPairs from 'lodash/toPairs'
import fromPairs from 'lodash/fromPairs'
import zip from 'lodash/zip'
import { largestRemainderRound  } from '../../util/mathUtils'

function formatLabels(data) {
  // Convert to list of key-value pairs
  const pairs = toPairs(data)
  // Convert from decimal fractions to percentages
  const percentages = pairs.map(([, item]) => item.value * 100)
  // Round to integers
  let rounded
  if (percentages.length === 4) {
    // When full data set (4 labels) ensure their rounded values sum to 100...
    rounded = largestRemainderRound(percentages, 100)
  } else {
    // ... Otherwise do regular rounding
    rounded = percentages.map(v => Math.round(v))
  }
  // Convert to percentage strings
  const formatted = rounded.map(value => `${value}%`)
  // Collect in object with original keys
  const newPairs = zip(pairs, formatted).map(([[key,], value]) => [key, value])
  return fromPairs(newPairs)
}

const DataLabels = ({ theme, scales, data, show, hatched, inverted }) => {
  if (!data) return null

  const { north, east, south, west } = scales
  const padding = 10
  const font = {
    fontFamily: theme.font3,
    fontSize: 14,
    fill: inverted ? theme.inverted.one[0] : theme.one[0],
    fontWeight: '900'
  }
  const bgStroke = {
    strokeWidth: '7px',  // This allows the text to be read clearly over darker/hatched background
    stroke: theme.tertiary
  }
  const formatted = formatLabels(data)

  let labels = []
  let bgLabels = []
  if (show.north) {
    const position = {
      x: (show.east ? (east(0) + padding) : (west(0) - padding)),
      y: (north(data.north.value) - padding),
      textAnchor: (show.east ? 'start' : 'end'),
      alignmentBaseline: 'baseline',
    }
    const content = formatted.north
    bgLabels.push(<text key="northBg" {...bgStroke} {...font} {...position}>{content}</text>)
    labels.push(<text className="north" key="north" {...font} {...position}>{content}</text>)
  }
  if (show.east) {
    const position = {
      x: (east(data.east.value) + padding),
      y: (show.south ? (south(0) + padding) : (north(0) - padding)),
      textAnchor: 'start',
      alignmentBaseline: (show.south ? 'hanging' : 'baseline'),
    }
    const content = formatted.east
    bgLabels.push(<text key="eastBg" {...bgStroke} {...font} {...position}>{content}</text>)
    labels.push(<text className="east" key="east" {...font} {...position}>{content}</text>)
  }
  if (show.south) {
    const position = {
      x: (show.west ? (west(0) - padding) : (east(0) + padding)),
      y: (south(data.south.value) + padding),
      textAnchor: (show.west ? 'end' : 'start'),
      alignmentBaseline: 'hanging',
    }
    const content = formatted.south
    bgLabels.push(<text key="southBg" {...bgStroke} {...font} {...position}>{content}</text>)
    labels.push(<text className="south" key="south" {...font} {...position}>{content}</text>)
  }
  if (show.west) {
    const position = {
      x: (west(data.west.value) - padding),
      y: (show.north ? (north(0) - padding) : (south(0) + padding)),
      textAnchor: 'end',
      alignmentBaseline: (show.north ? 'baseline' : 'hanging'),
    }
    const content = formatted.west
    bgLabels.push(<text key="westBg" {...bgStroke} {...font} {...position}>{content}</text>)
    labels.push(<text className="west" key="west" {...font} {...position}>{content}</text>)
  }

  return (
    <Fragment>
      {hatched ? <g>{bgLabels}</g> : null}
      <g>{labels}</g>
    </Fragment>
  )
}

const dataItemPropType = PropTypes.shape({
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
})

DataLabels.propTypes = {
  scales: PropTypes.shape({
    north: PropTypes.func.isRequired,
    east: PropTypes.func.isRequired,
    south: PropTypes.func.isRequired,
    west: PropTypes.func.isRequired,
  }).isRequired,
  data: PropTypes.shape({
    north: dataItemPropType,
    east: dataItemPropType,
    south: dataItemPropType,
    west: dataItemPropType,
  }).isRequired,
  show: PropTypes.shape({
    north: PropTypes.bool,
    east: PropTypes.bool,
    south: PropTypes.bool,
    west: PropTypes.bool
  }),
  inverted: PropTypes.bool
}

DataLabels.defaultProps = {
  show: {
    north: true,
    east: true,
    south: true,
    west: true
  },
  inverted: false
}

export default withTheme(DataLabels)
