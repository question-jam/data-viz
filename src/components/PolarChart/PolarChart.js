import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'
import * as d3 from 'd3'
import Border from './Border'
import CircleTicks from './CircleTicks'
import PolarAxes from './PolarAxes'
import DataPolygon from './DataPolygon'
import DataLabels from './DataLabels'
import QuadrantLabels from './QuadrantLabels'
import sortBy from 'lodash/sortBy'


const TICK_COUNT = 6


function dataMax({ north, east, south, west }) {
  return Math.max(north.value, east.value, south.value, west.value)
}

// Copy and add given domain to all scales
function applyScaleDomain(scales, domain) {
  const newScales = {}
  Object.keys(scales).forEach(key => {
    newScales[key] = scales[key].copy().domain(domain)
  })
  return newScales
}

// Initialize chart bounds
function initBounds(height, width, padding, show) {
  const bounds = {
    x: {
      min: padding,
      max: (width - padding)
    },
    y: {
      min: padding,
      max: (height - padding)
    }
  }

  const origin = {}
  if (show.west && show.east) {
    origin.x = width/2
  } else {
    origin.x = show.east ? bounds.x.min : bounds.x.max
  }
  if(show.north && show.south) {
    origin.y = height/2
  } else {
    origin.y = show.south ? bounds.y.min : bounds.y.max
  }

  return { origin, bounds }
}

// Initialize fluid scales utilizing all space between origin and bounds
function initFluidScales(origin, bounds) {
  return {
    north: d3.scaleLinear().range([origin.y, bounds.y.min]),
    east: d3.scaleLinear().range([origin.x, bounds.x.max]),
    south: d3.scaleLinear().range([origin.y, bounds.y.max]),
    west: d3.scaleLinear().range([origin.x, bounds.x.min])
  }
}

// Initialize synchronized scales where shortest axis sets a universal upper limit
function initSyncedScales(origin, bounds, padding) {
  const xLimit = (origin.x !== bounds.x.min && origin.x !== bounds.x.max) ? Math.abs(bounds.x.max - origin.x) : (bounds.x.max - bounds.y.min)
  const yLimit = (origin.y !== bounds.y.min && origin.y !== bounds.y.max) ? Math.abs(bounds.y.max - origin.y) : (bounds.y.max - bounds.y.min)
  const commonLimit = Math.min(xLimit, yLimit)
  return {
    north: d3.scaleLinear().range([origin.y, origin.y - commonLimit + padding]),
    east: d3.scaleLinear().range([origin.x, origin.x + commonLimit - padding]),
    south: d3.scaleLinear().range([origin.y, origin.y + commonLimit - padding]),
    west: d3.scaleLinear().range([origin.x, origin.x - commonLimit + padding]),
  }
}

// Initialize synchronized scales and assign a maximized domain (based on data)
function initFitScales(origin, bounds, padding, data) {
  let xLimit = (origin.x !== bounds.x.min && origin.x !== bounds.x.max) ? Math.abs(bounds.x.max - origin.x) : (bounds.x.max - bounds.y.min)
  let yLimit = (origin.y !== bounds.y.min && origin.y !== bounds.y.max) ? Math.abs(bounds.y.max - origin.y) : (bounds.y.max - bounds.y.min)
  xLimit = xLimit - padding
  yLimit = yLimit - padding

  const collectOverflow = (limit, value) => {
    const domain = [0, value]
    const maxScales = {
      north: d3.scaleLinear().range([origin.y, origin.y - limit]),
      east: d3.scaleLinear().range([origin.x, origin.x + limit]),
      south: d3.scaleLinear().range([origin.y, origin.y + limit]),
      west: d3.scaleLinear().range([origin.x, origin.x - limit]),
    }
    const scales = applyScaleDomain(maxScales, domain)
    const leftover = {}
    if (data.north) {
      leftover.north = yLimit - (scales.north(0) - scales.north(data.north.value))
    }
    if (data.east) {
      leftover.east = xLimit - (scales.east(data.east.value) - scales.east(0))
    }
    if (data.south) {
      leftover.south = yLimit - (scales.south(data.south.value) - scales.south(0))
    }
    if (data.west) {
      leftover.west = xLimit - (scales.west(0) - scales.west(data.west.value))
    }
    const hasOverflow = Object.values(leftover).some(v => v < 0)
    if (!hasOverflow) {
      return {
        leftover: Object.values(leftover).reduce((acc, v) => acc + v, 0),
        scales
      }
    }
  }

  let candidates = [
    { limit: yLimit, value: data.north && data.north.value },
    { limit: xLimit, value: data.east && data.east.value },
    { limit: yLimit, value: data.south && data.south.value },
    { limit: xLimit, value: data.west && data.west.value }
  ]
  candidates = candidates.filter(c => c.value && c.value > 0)
  let candidateList = candidates.map(c => collectOverflow(c.limit, c.value)).filter(c => c)
  const candidatesSorted = sortBy(candidateList, (c) => -c.leftover)

  return candidatesSorted[0].scales
}

// Extends the domain of all scales so that they start and end on nice round values
function makeScalesNice({ north, east, south, west }) {
  return {
    north: north.nice(TICK_COUNT),
    east: east.nice(TICK_COUNT),
    south: south.nice(TICK_COUNT),
    west: west.nice(TICK_COUNT),
  }
}

class PolarChart extends Component {
  render() {
    const { width, height, data, primary, inverted, show, hatched, round, labels, sum } = this.props

    const hasCircleTicks = round
    const hasOuterAxisLabels = round
    const hasQuadrantLabels = labels
    const hasBorder = !round
    const outerPadding = 13
    const { origin, bounds } = initBounds(height, width, outerPadding, show)
    const max = (data ? dataMax(data) : null)

    let axisScales, dataScales
    if (round && data) {
      const dataPadding = 60
      const domain = [0, (max + 0.05)]
      dataScales = initSyncedScales(origin, bounds, dataPadding)
      dataScales = applyScaleDomain(dataScales, domain)
      dataScales = makeScalesNice(dataScales)
      axisScales = dataScales
    } else {
      const dataPadding = 100
      axisScales = initFluidScales(origin, bounds)
      dataScales = max ? initFitScales(origin, bounds, dataPadding, data) : axisScales
    }

    return (
      <svg height={height} width={width}>
        <Border bounds={bounds} hatched={hatched} hidden={!hasBorder} />
        <CircleTicks origin={origin} scales={dataScales} hidden={!hasCircleTicks} inverted={inverted} />
        <PolarAxes scales={axisScales} show={show} outerLabels={hasOuterAxisLabels} inverted={inverted} />
        <DataPolygon scales={dataScales} data={data} show={show} sum={sum} primary={primary} inverted={inverted} />
        <DataLabels scales={dataScales} data={data} show={show} inverted={inverted} />
        <QuadrantLabels bounds={bounds} show={show} hidden={!hasQuadrantLabels} />
      </svg>
    )
  }
}

const dataDirection = {
  key: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired
}

PolarChart.propTypes = {
  data: PropTypes.shape({
    north: dataDirection,
    east: dataDirection,
    south: dataDirection,
    west: dataDirection
  }).isRequired,
  width: PropTypes.number,
  height: PropTypes.number,
  show: PropTypes.shape({
    north: PropTypes.bool,
    east: PropTypes.bool,
    south: PropTypes.bool,
    west: PropTypes.bool
  }),
  primary: PropTypes.bool,
  inverted: PropTypes.bool,
  round: PropTypes.bool,
  hatched: PropTypes.bool,
  sum: PropTypes.bool,
  labels: PropTypes.bool
}

PolarChart.defaultProps = {
  width: 560,
  height: 560,
  show: {
    north: true,
    east: true,
    south: true,
    west: true
  },
  primary:false,
  inverted: false,
  round:false,
  hatched: false,
  sum: false,
  labels: false
}

export default withTheme(PolarChart)
