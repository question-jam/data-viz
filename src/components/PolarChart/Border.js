import React from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'


const Border = ({ theme, bounds, hatched, hidden }) => {
  let pattern
  if (hidden) return null
  if (hatched) {
    pattern = (
      <pattern id="diagonalHatch" width="5" height="5" patternTransform="rotate(45 0 0)" patternUnits="userSpaceOnUse">
        <line
          x1="0"
          y1="0"
          x2="0"
          y2="5"
          stroke={theme.default}
          strokeWidth="2"
          strokeOpacity="0.1"
        />
      </pattern>
    )
  }
  return (
    <g>
      {pattern}
      <rect
        x={bounds.x.min}
        y={bounds.y.min}
        fill={hatched ? 'url(#diagonalHatch)' : 'transparent'}
        strokeWidth={2}
        stroke={theme.default}
        strokeOpacity="0.1"
        height={bounds.y.max - bounds.y.min}
        width={bounds.x.max - bounds.x.min}
      />
    </g>
  )
}

Border.propTypes = {
  bounds: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired
  }).isRequired,
  hatched: PropTypes.bool,
  hidden: PropTypes.bool
}

Border.defaultProps = {
  hatched: false,
  hidden: false
}

export default withTheme(Border)
