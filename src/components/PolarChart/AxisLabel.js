import React from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'
import * as d3 from 'd3'


function scaleMin(scale) {
  return scale(d3.min(scale.domain()))
}

function scaleMax(scale) {
  return scale(d3.max(scale.domain()))
}

function getPositionBasics(scaleX, scaleY, isHorizontal) {
  return {
    isInvertedX: (scaleMin(scaleX) > scaleMax(scaleX)),
    isInvertedY: (scaleMin(scaleY) < scaleMax(scaleY)),
    x: isHorizontal ? scaleMax(scaleX) : scaleMin(scaleX),
    y: isHorizontal ? scaleMin(scaleY) : scaleMax(scaleY)
  }
}

function positionInner(scaleX, scaleY, horizontal, isMultiLine = false) {
  const attr = {}
  const { isInvertedX, isInvertedY, x, y } = getPositionBasics(scaleX, scaleY, horizontal)

  if (horizontal) {
    attr.alignmentBaseline = isInvertedY ? 'hanging' : 'baseline'
  } else {
    attr.alignmentBaseline = isInvertedY ? 'baseline' : 'hanging'
  }

  if (horizontal) {
    attr.textAnchor = isInvertedX ? 'start' : 'end'
  } else {
    attr.textAnchor = isInvertedX ? 'end' : 'start'
  }

  const textPadding = 20
  let dx, dy
  if (horizontal) {
    dx = isInvertedX ? textPadding : -textPadding
    dy = isInvertedY ? textPadding : -textPadding
  } else {
    dx = isInvertedX ? -textPadding : textPadding
    dy = isInvertedY ? -textPadding : textPadding
  }
  attr.x = (x + dx)
  attr.y = (y + dy)

  if (isMultiLine) {
    dy = (isInvertedY ? 16 : -16)
    return [
      { ...attr, y: (attr.y + 2*dy) },
      { ...attr, y: (attr.y + dy) },
      attr
    ]
  } else {
    return attr
  }
}

function positionOuter(scaleX, scaleY, horizontal, isMultiLine = false) {
  const attr = {}
  const { isInvertedX, isInvertedY, x, y } = getPositionBasics(scaleX, scaleY, horizontal)

  if (horizontal) {
    attr.alignmentBaseline = 'middle'
  } else {
    attr.alignmentBaseline = isInvertedY ? 'hanging' : 'baseline'
  }

  if (horizontal) {
    attr.textAnchor = isInvertedX ? 'end' : 'start'
  } else {
    attr.textAnchor = 'middle'
  }

  const textPadding = 20
  let dx, dy
  if (horizontal) {
    dx = isInvertedX ? -textPadding : textPadding
    dy = 0
  } else {
    dx = 0
    dy = isInvertedY ? textPadding : -textPadding
  }
  attr.x = (x + dx)
  attr.y = (y + dy)

  if (isMultiLine) {
    dy = (isInvertedY ? 16 : -16)
    return [
      { ...attr, y: (attr.y - dy) },
      attr,
      { ...attr, y: (attr.y + dy) },
    ]
  } else {
    return attr
  }
}

const AxisLabel = ({ theme, xScale, yScale, label, outer, horizontal }) => {
  const style = {
    fontFamily: theme.font2,
    fontSize: '15px',
    fontWeight: '300',
    fill: theme.default,
    fillOpacity: '0.5'
  }
  const calcPosition = outer ? positionOuter : positionInner
  const isMultiLine = (label.split(' ').length === 3)
  if (isMultiLine) {
    const [ pos1, pos2, pos3 ] = calcPosition(xScale, yScale, horizontal, isMultiLine)
    const lines = label.split(' ')
    return (
      <g>
        <text {...pos1} {...style}>{lines[0]}</text>
        <text {...pos2} {...style}>{lines[1]}</text>
        <text {...pos3} {...style}>{lines[2]}</text>
      </g>
    )
  } else {
    return (
      <text {...calcPosition(xScale, yScale, horizontal)} {...style}>{label}</text>
    )
  }
}

AxisLabel.propTypes = {
  xScale: PropTypes.func.isRequired,
  yScale: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  outer: PropTypes.bool,
  horizontal: PropTypes.bool
}

AxisLabel.defaultProps = {
  outer: false,
  horizontal: false
}

export default withTheme(AxisLabel)
