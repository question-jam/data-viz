import React, { Component } from 'react'
import { withTheme } from 'styled-components'
import PropTypes from 'prop-types'
import * as d3 from 'd3'

class HorizontalTickAxis extends Component {
  render() {
    const { scale, tickCount, theme } = this.props

    const ticks = scale.ticks(tickCount)
    const markers = ticks.map((value, i) => (
      <g key={i} transform={`translate(${scale(value)}, 0)`}>
        <line stroke={theme.default} strokeWidth="2" y1="0" y2="6"/>
        {/*<text fill="#2E4657" x="0" dy="18" textAnchor="middle">{value}</text>*/}
      </g>
    ))

    const max = d3.max(scale.range())
    const bar = (
      <path
        key={-1}
        stroke={theme.default}
        strokeWidth="2"
        d={`M0,0 L${max},0`}
      />
    )

    return (
      <g>
        {markers}
        {bar}
      </g>
    )
  }
}
HorizontalTickAxis.propTypes = {
  scale: PropTypes.func.isRequired,
  tickCount: PropTypes.number
}

HorizontalTickAxis.defaultProps = {
  tickCount: 10
}

export default withTheme(HorizontalTickAxis)
