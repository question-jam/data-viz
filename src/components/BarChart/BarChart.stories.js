import React from 'react'
import styled from 'styled-components'
import { storiesOf } from '@storybook/react'

import BarChart from './BarChart'
import SingleBarChart from './SingleBarChart'
import Legend from '../Legend/Legend'

const MOCK_DATA = [{
  id: '1',
  value: 33,
  fill: '#65936e',
  label: 'Nurses',
}, {
  id: '2',
  value: 1,
  fill: '#ed9a91',
  label: 'Doctors',
}, {
  id: '3',
  value: 100,
  fill: '#f4bf6c',
  label: 'Experts',
}, {
  id: '4',
  value: 0,
  fill: '#486e88',
  label: 'Admins',
}, {
  id: '5',
  value: 60,
  fill: '#cd5154',
  label: 'Someone with really long name',
}, {
  id: '6',
  value: 10,
  fill: '#f7a15d',
  label: 'Come on with those group names damn it',
}]

const Wrapper = styled.div`
  padding: 20px;
  background: ${({ theme, inverted }) => inverted ? theme.main.secondary : theme.two[0]}; 
  width: ${({ width }) => width || 'auto'};
  ${({ isFlex }) => isFlex && `
    display: flex;
    flex-wrap: wrap;
  `};
`

const LimitedSpace = styled.div`
  width: 145px;
`

storiesOf('Data Visualization/BarChart', module)
  .add('Full', () => {
    return (
      <Wrapper>
        <BarChart
          data={MOCK_DATA}
          width={600}
        />
      </Wrapper>
    )
  })
  .add('Simple', () => {
    return (
      <Wrapper width="500px">
        <BarChart
          simple
          data={MOCK_DATA}
          width={460}
        />
      </Wrapper>
    )
  })
  .add('Minimal', () => {
    return (
      <Wrapper width="500px" inverted>
        <BarChart
          minimal
          data={MOCK_DATA}
          width={460}
        />
      </Wrapper>
    )
  })
  .add('Inverted', () => {
    return (
      <Wrapper width="500px" inverted>
        <BarChart
          minimal
          inverted
          data={MOCK_DATA}
          width={460}
        />
      </Wrapper>
    )
  })
  .add('With Legend', () => {
    return (
      <Wrapper width="500px">
        <BarChart
          simple
          data={MOCK_DATA}
          width={460}
        />
        <LimitedSpace>
          <Legend data={MOCK_DATA} />
        </LimitedSpace>
      </Wrapper>
    )
  })
  .add('With Legend (horizontal)', () => {
    return (
      <Wrapper width="500px">
        <BarChart
          simple
          data={MOCK_DATA}
          width={460}
        />
        <Legend data={MOCK_DATA} horizontal={true} />
      </Wrapper>
    )
  })
  .add('Only one highlighted', () => {
    return (
      <Wrapper isFlex>
        <Wrapper width="200px">
          <BarChart highlight data={[{ id: '1', value: 0, fill: '#65936e'}]} width={160} />
        </Wrapper>
        <Wrapper width="200px">
          <BarChart highlight data={[{ id: '1', value: 1, fill: '#65936e'}]} width={160} />
        </Wrapper>
        <Wrapper width="200px">
          <BarChart highlight data={[{ id: '1', value: 12, fill: '#65936e'}]} width={160} />
        </Wrapper>
        <Wrapper width="200px">
          <BarChart highlight data={[{ id: '1', value: 34, fill: '#65936e'}]} width={160} />
        </Wrapper>
        <Wrapper width="200px">
          <BarChart highlight data={[{ id: '1', value: 35, fill: '#65936e'}]} width={160} />
        </Wrapper>
        <Wrapper width="200px">
          <BarChart highlight data={[{ id: '1', value: 60, fill: '#65936e'}]} width={160} />
        </Wrapper>
        <Wrapper width="200px">
          <BarChart highlight data={[{ id: '1', value: 100, fill: '#65936e'}]} width={160} />
        </Wrapper>
      </Wrapper>
    )
  })
  .add('Single bar (10%)', () => {
    return (
      <Wrapper>
        <SingleBarChart width={300} value={10} level="LOW" />
      </Wrapper>
    )
  })
  .add('Single bar (66%)', () => {
    return (
      <Wrapper>
        <SingleBarChart width={300} value={66} level="MEDIUM" />
      </Wrapper>
    )
  })
  .add('Single bar (95%)', () => {
    return (
      <Wrapper>
        <SingleBarChart width={300} value={95} level="HIGH" />
      </Wrapper>
    )
  })
