import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'
import * as d3 from 'd3'
import zipWith from 'lodash/zipWith'

const DATA_TOP_PADDING = 75
const DATA_TOP_PADDING_MINIMAL = 45
const DATA_BOTTOM_PADDING = 25
const DATA_LEFT_PADDING = 15
const BAR_HEIGHT = 6
const BAR_GAP = 30
const BAR_TEXT_WIDTH_NORMAL = 30
const BAR_TEXT_WIDTH_LARGE = 50
const DESCRIPTION_CIRCLE_OUTER_RADIUS = 5

// the exact upper limit is not part of the interval, hence the 101 at the end
const PARTICIPATION_BREAKPOINTS = [{
  label: 'Low',
  description: '(0-29%)',
  interval: [0, 30],
  innerRadius: 4,
}, {
  label: 'Good',
  description: '(30-59%)',
  interval: [30, 60],
  innerRadius: 3,
}, {
  label: 'High',
  description: '(60+%)',
  interval: [60, 101],
  innerRadius: 0,
}]

class BarChart extends Component {
  renderChartDetails(width) {
    const { data, highlight, minimal } = this.props
    const ids = data.map(x => x.id)

    // height of bars and gaps in between. last bar height is excluded
    const yAxisHeight = (data.length - 1) * (BAR_HEIGHT + BAR_GAP)

    // width of the maximum bar
    const xAxisWidth = width - DATA_LEFT_PADDING

    // height of the grid. width is given by function input
    const dataTopPadding = minimal ? DATA_TOP_PADDING_MINIMAL : DATA_TOP_PADDING
    const height = yAxisHeight + BAR_HEIGHT + dataTopPadding + DATA_BOTTOM_PADDING

    // vertical draw scale (id -> y-coordinate)
    const scaleY = d3.scalePoint()
      .range([dataTopPadding, dataTopPadding + yAxisHeight])
      .domain(ids)

    // horizontal draw scale 1 (value -> x-coordinate)
    const scaleX = d3.scaleLinear()
      .range([0, xAxisWidth])
      .domain([0, 100])
      .nice(10)

    // render bars + bar background + bar text
    const originPx = scaleX(d3.min(scaleX.range()))
    const bars = data.map(({ value, id, fill }) => {
      const attrs = {
        key: id,
        value: value,
        x1: originPx,
        x2: scaleX(value),
        y: scaleY(id),
        xmax: xAxisWidth,
        fill,
      }
      return this.renderBar(attrs)
    })


    // compose SVG element
    return (
      <svg width={width} height={height}>
        {highlight
          // describe first element only
          ? this.renderDescription(data[0], height)
          // one is subtracted from the grid height
          // in order to draw the bottom axis inside the SVG element
          : this.renderGrid(height - 1, xAxisWidth + DATA_LEFT_PADDING, scaleX)}
        {bars}
      </svg>
    )
  }

  renderDescription = ({ value }, height) => {
    // decide which description is the right fit
    const { innerRadius, label } = PARTICIPATION_BREAKPOINTS
      .find(({ interval: [lower, upper] }) => lower <= value && value < upper)
    return (
      <g key="grid" transform={`translate(20, ${height})`} >
        <circle cy="-8" r={DESCRIPTION_CIRCLE_OUTER_RADIUS} fill="#f4bf6c" />
        <circle cy="-8" r={innerRadius} fill="white" />
        <text
          x="15"
          dy="-3"
          fontFamily="Raleway, sans-serif"
          fontSize="13px"
          fill="#3d4640"
          fontWeight="700"
        >
          {label}
        </text>
      </ g>
    )
  }

  renderGrid = (height, width, scaleX) => {
    const { theme, minimal, inverted } = this.props
    let simple = minimal || this.props.simple
    //const descriptionColor = this.props.primary ? theme.main.primary : theme.one[0] // TODO: Discuss with Oliver whether we really need this complexity
    const headingColor = inverted ? theme.inverted.one[0] : theme.one[0]
    const subHeadingColor = inverted ? theme.inverted.one[2] : theme.one[2]
    const pointColor = inverted ? theme.inverted.one[0] : theme.one[0]
    const borderColor = inverted ? theme.one[1] : theme.one[1]

    // scale breakpoints and add them to the constant list
    const offsets = [0, scaleX(30) + DATA_LEFT_PADDING, scaleX(60) + DATA_LEFT_PADDING]
    const gridBreakpoints = zipWith(
      PARTICIPATION_BREAKPOINTS, offsets,
      (breakpoints, offset) => ({ ...breakpoints, offset })
    )

    // simple charts have their percentage descriptions moved below the text
    const descriptionProps = simple ? { x: 40, dy: 20, } : {}
    const markers = gridBreakpoints.map(({ label, offset, description, innerRadius }) => (
      <g key={label} transform={`translate(${offset}, 0)`}>
        <rect fill={borderColor} x="-4" width="9" height="1" />
        <rect fill={borderColor} width="1" height={height} />
        <mask id={'marker-clip-' + label}>
          <circle cx="20" cy="15" r={DESCRIPTION_CIRCLE_OUTER_RADIUS} fill="white" />
          <circle cx="20" cy="15" r={innerRadius} fill="black" />
        </mask>
        <circle cx="20" cy="15" r={DESCRIPTION_CIRCLE_OUTER_RADIUS} fill={pointColor} mask={`url(#${'marker-clip-' + label})`} />

        <text x="40" y="20" alignmentBaseline="hanging">
          <tspan
            fontFamily="Raleway, sans-serif"
            fontSize="13px"
            fill={headingColor}
            fontWeight="700"
          >
            {label}
          </tspan>
          &nbsp;&nbsp;&nbsp;
          {!minimal ? <tspan
            {...descriptionProps}
            fontFamily="LL Circular Book Web, sans-serif"
            fontSize="12px"
            fill={subHeadingColor}
            fontWeight="500"
          >
            {description}
          </tspan> : null}
        </text>
      </g>
    ))

    return (
      <g key="grid">
        {/* render markers */}
        {markers}
        {/* bottom line */}
        <g key="bottom" transform={`translate(0, ${height})`}>
          <rect fill={borderColor} width={width} height="1" />
        </g>
      </g>
    )
  }

  renderBar = ({ fill, x1, x2, y, value, key, xmax }) => {
    const { theme, inverted } = this.props
    const backgroundColor = inverted ? theme.inverted.two[0] : theme.inverted.one[0]
    return (
      <g key={key} transform={`translate(${DATA_LEFT_PADDING}, 0)`}>
        {/* render bar background */}
        <rect x={x1} y={y} width={xmax} height={BAR_HEIGHT} rx="4" fill={backgroundColor} />
        {/* render bar */}
        <rect x={x1} y={y} width={x2 - x1} height={BAR_HEIGHT} rx="4" fill={fill} />
        {/* render text above bar */}
        {this.renderBarText(x1, x2, y, value)}
      </g>
    )
  }

  renderBarText = (x1, x2, y, value) => {
    const { simple, minimal, highlight } = this.props
    // simplified chart does not display any text above the bar
    if (simple || minimal) return null
    // regular chart renders normal size percentages
    if (!highlight) {
      return (
        <text
          x={x2}
          y={y - BAR_HEIGHT}
          fill="#3d4640"
          fontFamily="LL Circular Black Web, sans-serif"
          fontWeight="900"
          fontSize="15px"
          textAnchor={(x2 - x1) < BAR_TEXT_WIDTH_NORMAL ? 'start' : 'end'}
        >
          {value}%
        </text>
      )
    }
    // if highlighted render large signs
    return (
      <text
        x={x2}
        y={y - 20}
        fill="#3d4640"
        fontFamily="LL Circular Book Web, sans-serif"
        fontWeight="500"
        fontSize="34px"
        textAnchor={(x2 - x1) < BAR_TEXT_WIDTH_LARGE ? 'start' : 'end'}
      >
        {value}
        <tspan fontSize="18px">%</tspan>
      </text>
    )
  }


  render() {
    return this.renderChartDetails(this.props.width)
  }
/*
  FIXME: Move this wrapper out of data-viz entirely
  render() {
    if (!this.props.data) return null
    return (
      <div>
        <ContainerDimensions>
          {({ width }) => this.renderChartDetails(width)}
        </ContainerDimensions>
      </div>
    )
  }
 */
}

BarChart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    ids: PropTypes.string,
    fill: PropTypes.string,
    values: PropTypes.number,
  })),
  minimal: PropTypes.bool,
  simple: PropTypes.bool,
  inverted: PropTypes.bool,
  highlight: PropTypes.bool,
}

BarChart.defaultProps = {
  minimal: false,
  simple: false,
  inverted: false,
  highlight: false,
}

export default withTheme(BarChart)
