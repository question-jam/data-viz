import React from 'react'
import { withTheme } from 'styled-components'
import * as d3 from 'd3'
import PropTypes from 'prop-types'


const FONT_SIZE = 16
const TEXT_Y = 21
const BAR_HEIGHT = 30
const BAR_TEXT_WIDTH_NORMAL = 50

function determineColor(theme, level) {
  if (level == 'HIGH') {
    return theme.alert.ok
  } else if (level === 'MEDIUM') {
    return theme.alert.warning
  }
  return theme.alert.error
}

const BarText = withTheme(({ x1, x2, y, value, theme }) => {
  const isLeftAligned = ((x2 - x1) < BAR_TEXT_WIDTH_NORMAL)
  const dx = isLeftAligned ? 10 : -10
  const fill = isLeftAligned ? theme.one[0] : theme.two[0]
  return (
    <text
      x={x2 + dx}
      y={TEXT_Y}
      fill={fill}
      fontFamily={theme.font3}
      fontWeight="900"
      fontSize={FONT_SIZE}
      textAnchor={isLeftAligned ? 'start' : 'end'}
    >
      {value}%
    </text>
  )
})

const Bar = withTheme(({ fill, x1, x2, y, value, xmax, theme }) => {
  return (
    <g>
      {/* render bar background */}
      <rect x={x1} y={y} width={xmax} height={BAR_HEIGHT} rx="4" fill={theme.inverted.one[0]} />
      {/* render bar */}
      <rect x={x1} y={y} width={x2 - x1} height={BAR_HEIGHT} rx="4" fill={fill} />
      {/* render text above bar */}
      <BarText x1={x1} x2={x2} y={y} value={value} />
    </g>
  )
})

const SingleBarChart = ({ width, value, level, theme }) => {
  // horizontal draw scale 1 (value -> x-coordinate)
  const scaleX = d3.scaleLinear()
    .range([0, width])
    .domain([0, 100])
    .nice(10)

  // render bars + bar background + bar text
  const originPx = scaleX(d3.min(scaleX.range()))

  // compose SVG element
  return (
    <svg width={width} height={BAR_HEIGHT}>
      <Bar
        value={value}
        x1={originPx}
        x2={scaleX(value)}
        xmax={width}
        y={0}
        fill={determineColor(theme, level)}
      />
    </svg>
  )
}

SingleBarChart.propTypes = {
  width: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  level: PropTypes.oneOf(['LOW', 'MEDIUM', 'HIGH']).isRequired
}

export default withTheme(SingleBarChart)
