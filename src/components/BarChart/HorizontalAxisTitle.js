import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'
import * as d3 from 'd3'

class HorizontalAxisTitle extends Component {
  render() {
    const { scale, theme, title, subtitle } = this.props
    const x = d3.median(scale.range())
    const attr = {
      x,
      fill: theme.default,
      fontFamily: theme.font1,
      fontSize: 13,
      textAnchor: 'middle'
    }
    const attrTitle = {
      ...attr,
      y: 25,
      fontWeight: 900
    }
    const attrSubTitle = {
      ...attr,
      y: 38,
      fontWeight: 500
    }
    return (
      <g>
        <text key={1} {...attrTitle}>{title}</text>,
        <text key={2} {...attrSubTitle}>{subtitle}</text>
      </g>
    )
  }
}

HorizontalAxisTitle.propTypes = {
  scale: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string
}

export default withTheme(HorizontalAxisTitle)
