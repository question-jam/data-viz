import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import styled from 'styled-components'
import * as theme from '../theme'
import { makeGroupColorScales } from '../../util/d3Utils'
import GroupsNetworkGraph from './GroupsNetworkGraph'

import dataAIGlobal from './data/aiglobal_graph'
import dataBusinessRegionAarshus from './data/nytaarskur_graph'
import dataAgillic from './data/agillic_graph'
import dataInstitutionsledereLandsdel from './data/ledelseskommissionen_institutionsledere_landsdel_graph'
import dataInstitutionsledereStatRegionKommune from './data/ledelseskommissionen_institutionsledere_statregionkommune_graph'
import dataTopledereLandsdel from './data/ledelseskommissionen_topledere_landsdel_graph'
import dataTopledereStilling from './data/ledelseskommissionen_topledere_stilling_graph'
import dataMariehjemmene from './data/mariehjemmene_graph'
import dataCHIArbejdssted from './data/chi_arbejdssted_graph'
import dataCHIFunktion from './data/chi_funktion_graph'
import dataKommunikation from './data/kommunikation_graph'
import dataAlleitaleOrg from './data/alleitale_org_tilknytning_graph'
import dataAlleitaleJob from './data/alleitale_job_graph'
import dataKbhKommuneOmraade from './data/kobenhavnskommune_omraade'
import dataKbhKommuneArbejdsomraade from './data/kobenhavnskommune_arbejdsomraade'
import dataKbhKommuneAlder from './data/kobenhavnskommune_alder'
import dataHROrgTitel from './data/hrogorganization_titel'
import dataHROrgTidligereEnhed from './data/hrogorganization_tidligere_enhed'
import dataATPLokation from './data/atp_lokation'
import dataATPFunktion from './data/atp_funktion'
import dataFodevarerMinist from './data/fodevareministeriet_typekategori'
import dataNorddjursDanishLevel from './data/norddjurskommune_danishlevel'
import dataNorddjursAge from './data/norddjurskommune_age'
import dataFirstQvest from './data/qvest_graph'
import dataDisconnected from './data/disconnected_graph'
import dataGlobalHr from './data/global_hr'
import themesGlobalHr from './data/globalhr_themes'

const CURRENT_NODE_COLLISION = 60

const InvertedBackground = styled.div`
  background-color: ${({ theme }) => theme.inverted.two[0]};
`

function makeNodeColors(data) {
  const ids = data.nodes.map(n => n.id)
  return makeGroupColorScales(theme.yellow, ids).primaryColors
}

function makeThemeColors(themes) {
  const ids = themes.map(t => t.themeId)
  return makeGroupColorScales(theme.yellow, ids).secondaryColors
}

storiesOf('Data Visualization/GroupsNetworkGraph', module)
  .add('Qvest.io - Minor link force', () => {
    const colors = makeNodeColors(dataFirstQvest)
    return (
      <GroupsNetworkGraph
        colors={colors}
        data={dataFirstQvest}
        linkForceDistance={100}
      />
    )
  })
  .add('Qvest.io - Link force', () => {
    const colors = makeNodeColors(dataFirstQvest)
    return (
      <GroupsNetworkGraph
        colors={colors}
        data={dataFirstQvest}
        linkForceDistance={200}
      />
    )
  })
  .add('Qvest.io - Minor node collision', () => {
    const colors = makeNodeColors(dataFirstQvest)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={40}
        colors={colors}
        data={dataFirstQvest}
      />
    )
  })
  .add('Qvest.io - Node collision', () => {
    const colors = makeNodeColors(dataFirstQvest)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataFirstQvest}
      />
    )
  })
  .add('Qvest.io - Node collision with curved edges', () => {
    const colors = makeNodeColors(dataFirstQvest)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={75}
        hasCurvedEdges={true}
        colors={colors}
        data={dataFirstQvest}
      />
    )
  })
  .add('Alle i tale - Minor link force', () => {
    const colors = makeNodeColors(dataAlleitaleOrg)
    return (
      <GroupsNetworkGraph
        colors={colors}
        data={dataAlleitaleOrg}
        linkForceDistance={100}
      />
    )
  })
  .add('Alle i tale - Link force', () => {
    const colors = makeNodeColors(dataAlleitaleOrg)
    return (
      <GroupsNetworkGraph
        colors={colors}
        data={dataAlleitaleOrg}
        linkForceDistance={200}
      />
    )
  })
  .add('Alle i tale - Minor node collision', () => {
    const colors = makeNodeColors(dataAlleitaleOrg)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={40}
        colors={colors}
        data={dataAlleitaleOrg}
      />
    )
  })
  .add('Alle i tale - Node collision', () => {
    const colors = makeNodeColors(dataAlleitaleOrg)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataAlleitaleOrg}
      />
    )
  })
  .add('Alle i tale - Node collision with curved edges', () => {
    const colors = makeNodeColors(dataAlleitaleOrg)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={75}
        hasCurvedEdges={true}
        colors={colors}
        data={dataAlleitaleOrg}
      />
    )
  })
  .add('Nytårskur - Minor link force', () => {
    const colors = makeNodeColors(dataBusinessRegionAarshus)
    return (
      <GroupsNetworkGraph
        colors={colors}
        data={dataBusinessRegionAarshus}
        linkForceDistance={100}
      />
    )
  })
  .add('Nytårskur - Link force', () => {
    const colors = makeNodeColors(dataBusinessRegionAarshus)
    return (
      <GroupsNetworkGraph
        colors={colors}
        data={dataBusinessRegionAarshus}
        linkForceDistance={200}
      />
    )
  })
  .add('Nytårskur - Minor node collision', () => {
    const colors = makeNodeColors(dataBusinessRegionAarshus)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={40}
        colors={colors}
        data={dataBusinessRegionAarshus}
      />
    )
  })
  .add('Nytårskur - Node collision', () => {
    const colors = makeNodeColors(dataBusinessRegionAarshus)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataBusinessRegionAarshus}
      />
    )
  })
  .add('Nytårskur - Node collision with curved edges', () => {
    const colors = makeNodeColors(dataBusinessRegionAarshus)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={75}
        hasCurvedEdges={true}
        colors={colors}
        data={dataBusinessRegionAarshus}
      />
    )
  })
  .add('AIGlobal', () => {
    const colors = makeNodeColors(dataAIGlobal)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataAIGlobal}
      />
    )
  })
  .add('Agillic', () => {
    const colors = makeNodeColors(dataAgillic)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataAgillic}
      />
    )
  })
  .add('Ledelseskommissionen - Institutionsledere - Landsdel', () => {
    const colors = makeNodeColors(dataInstitutionsledereLandsdel)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataInstitutionsledereLandsdel}
      />
    )
  })
  .add('Ledelseskommissionen - Institutionsledere - Stat, Region eller Kommune', () => {
    const colors = makeNodeColors(dataInstitutionsledereStatRegionKommune)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataInstitutionsledereStatRegionKommune}
      />
    )
  })
  .add('Ledelseskommissionen - Topledere - Stilling', () => {
    const colors = makeNodeColors(dataTopledereStilling)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataTopledereStilling}
      />
    )
  })
  .add('Ledelseskommissionen - Topledere - Landsdel', () => {
    const colors = makeNodeColors(dataTopledereLandsdel)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataTopledereLandsdel}
      />
    )
  })
  .add('Mariehjemmene', () => {
    const colors = makeNodeColors(dataMariehjemmene)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataMariehjemmene}
      />
    )
  })
  .add('Copenhagen Health Innovation - Arbejdssted', () => {
    const colors = makeNodeColors(dataCHIArbejdssted)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataCHIArbejdssted}
      />
    )
  })
  .add('Copenhagen Health Innovation - Funktion', () => {
    const colors = makeNodeColors(dataCHIFunktion)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataCHIFunktion}
      />
    )
  })
  .add('Kommunikation', () => {
    const colors = makeNodeColors(dataKommunikation)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataKommunikation}
      />
    )
  })
  .add('Alle i tale - Organisatorisk tilknytning', () => {
    const colors = makeNodeColors(dataAlleitaleOrg)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataAlleitaleOrg}
      />
    )
  })
  .add('Alle i tale - Job', () => {
    const colors = makeNodeColors(dataAlleitaleJob)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataAlleitaleJob}
      />
    )
  })
  .add('Københavns Kommune - Område', () => {
    const colors = makeNodeColors(dataKbhKommuneOmraade)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataKbhKommuneOmraade}
      />
    )
  })
  .add('Københavns Kommune - Arbejdsområde', () => {
    const colors = makeNodeColors(dataKbhKommuneArbejdsomraade)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataKbhKommuneArbejdsomraade}
      />
    )
  })
  .add('Københavns Kommune - Alder', () => {
    const colors = makeNodeColors(dataKbhKommuneAlder)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataKbhKommuneAlder}
      />
    )
  })
  .add('HR- og Organisationsudvikling - Titel', () => {
    const colors = makeNodeColors(dataHROrgTitel)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataHROrgTitel}
      />
    )
  })
  .add('HR- og Organisationsudvikling - Tidligere enhed', () => {
    const colors = makeNodeColors(dataHROrgTidligereEnhed)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataHROrgTidligereEnhed}
      />
    )
  })
  .add('ATP - Lokation', () => {
    const colors = makeNodeColors(dataATPLokation)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataATPLokation}
      />
    )
  })
  .add('ATP - Funktion', () => {
    const colors = makeNodeColors(dataATPFunktion)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataATPFunktion}
      />
    )
  })
  .add('Miljø- og Fødevareministeriet - Type/Kategori', () => {
    const colors = makeNodeColors(dataFodevarerMinist)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataFodevarerMinist}
      />
    )
  })
  .add('Norddjurs Kommune - Danish level', () => {
    const colors = makeNodeColors(dataNorddjursDanishLevel)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataNorddjursDanishLevel}
      />
    )
  })
  .add('Norddjurs Kommune - Age', () => {
    const colors = makeNodeColors(dataNorddjursAge)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataNorddjursAge}
      />
    )
  })
  .add('Test: Disconnected node', () => {
    const colors = makeNodeColors(dataDisconnected)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataDisconnected}
      />
    )
  })
  .add('Primary', () => {
    return (
      <div>
        <GroupsNetworkGraph
          height={500}
          width={500}
          linkForceDistance={300}
          data={dataGlobalHr}
        />
      </div>
    )
  })
  .add('Secondary', () => {
    return (
      <div>
        <GroupsNetworkGraph
          secondary
          height={500}
          width={500}
          linkForceDistance={300}
          data={dataGlobalHr}
        />
      </div>
    )
  })
  .add('Tertiary', () => {
    return (
      <div>
        <GroupsNetworkGraph
          tertiary
          height={500}
          width={500}
          linkForceDistance={300}
          data={dataGlobalHr}
        />
      </div>
    )
  })
  .add('Inverted primary', () => {
    return (
      <InvertedBackground>
        <GroupsNetworkGraph
          inverted
          height={500}
          width={500}
          linkForceDistance={300}
          data={dataGlobalHr}
        />
      </InvertedBackground>
    )
  })
  .add('Inverted secondary', () => {
    return (
      <InvertedBackground>
        <GroupsNetworkGraph
          inverted
          secondary
          height={500}
          width={500}
          linkForceDistance={300}
          data={dataGlobalHr}
        />
      </InvertedBackground>
    )
  })
  .add('Inverted tertiary', () => {
    return (
      <InvertedBackground>
        <GroupsNetworkGraph
          inverted
          tertiary
          height={500}
          width={500}
          linkForceDistance={300}
          data={dataGlobalHr}
        />
      </InvertedBackground>
    )
  })
  .add('Small', () => {
    return (
      <div>
        <GroupsNetworkGraph
          height={250}
          width={250}
          linkForceDistance={300}
          size="small"
          data={dataGlobalHr}
        />
      </div>
    )
  })
  .add('with edge colors (themes)', () => {
    const edgeColors = makeThemeColors(themesGlobalHr)
    return (
      <InvertedBackground>
        <GroupsNetworkGraph
          inverted
          tertiary
          height={500}
          width={800}
          showGroupNames
          showNodeTooltips={false}
          showEdgeTooltips={true}
          linkForceDistance={300}
          edgeColors={edgeColors}
          themes={themesGlobalHr}
          data={dataGlobalHr}
        />
      </InvertedBackground>
    )
  })
  .add('with one edge missing color (theme)', () => {
    const edges = [].concat(dataGlobalHr.edges)
    edges[2] = {
      ...edges[2],
      themeId: null,
      themes: null
    }
    const edgeColors = makeThemeColors(themesGlobalHr)
    return (
      <InvertedBackground>
        <GroupsNetworkGraph
          inverted
          tertiary
          height={500}
          width={800}
          showGroupNames
          showNodeTooltips={false}
          showEdgeTooltips={true}
          linkForceDistance={300}
          edgeColors={edgeColors}
          themes={themesGlobalHr}
          data={{ ...dataGlobalHr, edges }}
        />
      </InvertedBackground>
    )
  })
  .add('Draggable', () => {
    const colors = makeNodeColors(dataAIGlobal)
    return (
      <GroupsNetworkGraph
        draggable
        showNodeTooltips={false}
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        colors={colors}
        data={dataAIGlobal}
        onDragEnd={action('Drag end')}
        onEdgeClick={action('Edge clicked')}
      />
    )
  })
  .add('With coordinates', () => {
    const colors = makeNodeColors(dataAIGlobal)
    return (
      <GroupsNetworkGraph
        nodeCollideRadius={CURRENT_NODE_COLLISION}
        nodeCoordinates={{
          '4873': { x: 353, y: 272 },
          '4886': { x: 251, y: 208 },
          '4891': { x: 348, y: 391 },
          '4902': { x: 246, y: 327 }
        }}
        colors={colors}
        data={dataAIGlobal}
      />
    )
  })
