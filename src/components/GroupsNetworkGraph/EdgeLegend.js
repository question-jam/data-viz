import React from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components'


const ComponentRoot = styled.ul`
  padding: 0;
  margin: 0;
  list-style-type: none;
`

const LegendItem = styled.li`
  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-left: 20px;
  & > * {
    margin-right: 4px;
  }
`
const LegendItemLine = styled(LegendItem)`
  &::before {
    content: "";
    width: 12px;
    height: 4px;
    border-radius: 2px;
    background-color: ${({ fill }) => fill};
    position: absolute;
    top: 10px;
    left: 0;
  }
`

/*
TODO: Combine node and edge legends?
const LegendItemCircle = styled(LegendItem)`
  &::before {
    content: "";
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background-color: ${({ fill }) => fill};
    position: absolute;
    top: 9px;
    left: 0;
  }
`
*/

const EdgeLegend = (props) => {
  const { edge, colors, themes, inverted } = props
  const items = edge.themes
    .sort((a, b) => b.count - a.count)
    .map(edgeTheme => {
      const theme = themes.find(t => t.themeId === edgeTheme.themeId)
      return {
        ...edgeTheme,
        color: colors(edgeTheme.themeId),
        name: theme ? theme.name : ''
      }
    })
  return (
    <ComponentRoot>
      {items.map((item) => (
        <LegendItemLine key={item.themeId} fill={item.color}>
          <span>{item.name} ({item.count})</span>
        </LegendItemLine>
      ))}
    </ComponentRoot>
  )
}

EdgeLegend.propTypes = {
  edge: PropTypes.shape({
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired,
    themes: PropTypes.arrayOf(PropTypes.shape({
      themeId: PropTypes.string.isRequired,
      count: PropTypes.number.isRequired
    })),
  }).isRequired,
  themes: PropTypes.arrayOf(PropTypes.shape({
    themeId: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  })),
  inverted: PropTypes.bool
}

EdgeLegend.defaultProps = {
  inverted: false
}

export default withTheme(EdgeLegend)
