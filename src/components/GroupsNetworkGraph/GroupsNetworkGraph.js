import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components'
import { FormattedMessage } from 'react-intl'
import { drag as d3Drag } from 'd3-drag'
import { select as d3Select, event as d3Event } from 'd3-selection'
import { forceSimulation, forceLink, forceCollide, forceManyBody, forceCenter } from 'd3-force'
import { scaleLinear } from 'd3-scale'
import { areaRelativeRadiusScale } from '../../util/geometryUtils'
import { makeGroupColorScales } from '../../util/d3Utils'
import { requestAnimationFrame, cancelAnimationFrame } from '../../util/animationUtils'

import Tooltip from './Tooltip'
import EdgeLegend from './EdgeLegend'


const EdgeGroup = styled.g`
  cursor: ${({ isClickable }) => isClickable ? 'pointer' : 'default'};
  &:hover > .arrow {
    ${({ isClickable }) => isClickable ? 'fill: lightgray;' : ''};
  }
  &:hover > g > .link {
    ${({ isClickable }) => isClickable ? 'stroke: lightgray;' : ''};
  }
`

const NODE_RADIUS_RANGE = {
  small: { minRadius: 1.5, maxRadius: 15 },
  regular: { minRadius: 3, maxRadius: 30 },
}

const LINK_SIZE_FACTOR = {
  small: { minWidth: 1.5, factor: 6 },
  regular: { minWidth: 3, factor: 12 }
}

const ARROW_SIZE_RANGE = {
  small: [2, 6],
  regular: [4, 10]
}

const gMinLinkWidth = 3

function prepareLinkWidthScale(minLinkValue, maxLinkValue, minWidth, linkFactor) {
  return (value) => {
    if ((value - minLinkValue) !== 0 && (maxLinkValue - minLinkValue) !== 0) {
      return gMinLinkWidth + (linkFactor * (value - minLinkValue) / (maxLinkValue - minLinkValue))
    } else {
      return gMinLinkWidth
    }
  }
}

function prepareArrowWidthScale(minLinkValue, maxLinkValue, arrowSizeRange) {
  return scaleLinear()
    .domain([minLinkValue, maxLinkValue])
    .range(arrowSizeRange)
}

// Compute min/max values across entire data set
function maxima(nodes, edges) {
  const nodeValues = nodes.map(n => n.value && JSON.parse(n.value))
  const minNodeValue = nodeValues.reduce((min, v) => v < min ? v : min, Number.MAX_SAFE_INTEGER)
  const maxNodeValue = nodeValues.reduce((max, v) => v > max ? v : max, 0)
  const linkValues = edges.map(e => e.value && JSON.parse(e.value))
  const minLinkValue = linkValues.reduce((min, v) => v < min ? v : min, Number.MAX_SAFE_INTEGER)
  const maxLinkValue = linkValues.reduce((max, v) => v > max ? v : max, 0)
  return {
    minNodeValue,
    maxNodeValue,
    minLinkValue,
    maxLinkValue
  }
}

function determineLinkColor(edgeColors, tertiary, inverted, theme) {
  if (edgeColors) {
    const fallback = determineLinkColor(null, tertiary, inverted, theme)()
    return edgeColors.unknown(fallback)
  }
  if (tertiary) {
    return () => theme.one[1]
  }
  if (inverted) return () => theme.inverted.one[0]
  return () => theme.one[0]
}

function determineNodeFill({ colors, secondary, tertiary, inverted, theme }) {
  if (colors) return colors
  if (secondary) {
    return () => 'transparent'
  }
  if (tertiary) {
    return () => 'url(#diagonalNodeHatch)'
  }
  if (inverted) return () => theme.inverted.one[0]
  return () => theme.one[0]
}

function determineNodeStroke({ colors, secondary, tertiary, inverted, theme }) {
  if (colors) return colors
  if (secondary) {
    return inverted ? () => theme.inverted.one[0] : () => theme.one[0]
  }
  if (tertiary) {
    return () => theme.one[1]
  }
  return () => 'transparent'
}

// Render path of a given link from a source node to a target node
function linkPath(sourceNode, targetNode, nodeRadiusScale, hasCurvedEdges, normalizedValue, isUnidirectional) {
  // Deal with edges pointing at self
  let path
  const offsetOffset = normalizedValue * 1.1
  let sourceOffsetX = 0
  let sourceOffsetY = 0
  let targetOffsetX = 0
  let targetOffsetY = 0
  const sourceRadius = nodeRadiusScale(sourceNode.value)
  const targetRadius = nodeRadiusScale(targetNode.value)

  if (sourceNode.id === targetNode.id) {
    // Edges pointing at self
    const radius = sourceRadius
    const degrees57 = 1

    sourceOffsetX = -radius
    sourceOffsetY = 0
    targetOffsetX = -(radius * Math.cos(degrees57))
    targetOffsetY = -(radius * Math.sin(degrees57))

    const sourceControlX = -(radius * 2.5)
    const sourceControlY = 0
    const targetControlX = -((radius * 2.5) * Math.cos(degrees57))
    const targetControlY = -((radius * 2.5) * Math.sin(degrees57))

    path = `M${sourceNode.x + sourceOffsetX},${sourceNode.y + sourceOffsetY}C${sourceNode.x + sourceControlX} ${sourceNode.y + sourceControlY},${targetNode.x + targetControlX} ${targetNode.y + targetControlY},${targetNode.x + targetOffsetX},${targetNode.y + targetOffsetY}`
  } else {
    // Edges between nodes

    // Total difference in x and y from source to target
    const diffX = targetNode.x - sourceNode.x
    const diffY = targetNode.y - sourceNode.y

    // Length of path from center of source node to center of target node
    const pathLength = Math.sqrt((diffX * diffX) + (diffY * diffY))

    // x and y distances from center to outside edge of target node
    if (pathLength > 0) {
      const sourceGeneralOffset = isUnidirectional ? offsetOffset : 0
      const targetGeneralOffset = offsetOffset
      sourceOffsetX = (diffX * (sourceRadius + sourceGeneralOffset)) / pathLength
      sourceOffsetY = (diffY * (sourceRadius + sourceGeneralOffset)) / pathLength
      targetOffsetX = (diffX * (targetRadius + targetGeneralOffset)) / pathLength
      targetOffsetY = (diffY * (targetRadius + targetGeneralOffset)) / pathLength
    }

    const sourceX = sourceNode.x + sourceOffsetX
    const sourceY = sourceNode.y + sourceOffsetY
    const targetX = targetNode.x - targetOffsetX
    const targetY = targetNode.y - targetOffsetY

    if (hasCurvedEdges) {
      // Curved edges
      path = `M${sourceX},${sourceY}A${pathLength},${pathLength} 0 0,1 ${targetX},${targetY}`
    } else {
      // Direct edges
      path = `M${sourceX},${sourceY} L${targetX},${targetY}`
    }
  }
  return path
}

// Bounding box force (Constrains graph within box)
function boxForce(nodes, height, width, nodeRadiusScale) {
  let forceFunction = {}

  const setForceFunction = () => {
    forceFunction = () => {
      for (let i = 0, n = nodes.length; i < n; ++i) {
        let curr_node = nodes[i]
        const padding = 20
        const radius = nodeRadiusScale(curr_node.value) + padding
        const buffer = radius + padding
        curr_node.x = Math.max(buffer, Math.min(width - buffer, curr_node.x))
        curr_node.y = Math.max(buffer, Math.min(height - buffer, curr_node.y))
      }
    }
  }

  setForceFunction()

  forceFunction.height = (value) => {
    if (value) {
      height = value
      setForceFunction()
    }
    return height
  }

  forceFunction.width = (value) => {
    if (value) {
      width = value
      setForceFunction()
    }
    return width
  }
  return forceFunction
}

// Arrow heads at end of links (edges)
function arrowHeads(nodeSource, nodeTarget, edgeValue, arrowWidthScale, nodeRadiusScale) {
  const normalizedValue = arrowWidthScale(edgeValue)
  const s = normalizedValue * 1.2

  const sourceRadius = nodeRadiusScale(nodeSource.value)
  const targetRadius = nodeRadiusScale(nodeTarget.value)

  const offsetOffset = normalizedValue * 1.0

  const diffX = nodeTarget.x - nodeSource.x
  const diffY = nodeTarget.y - nodeSource.y

  const pathLength = Math.sqrt((diffX * diffX) + (diffY * diffY))

  const angle = Math.atan2(nodeTarget.y - nodeSource.y, nodeTarget.x - nodeSource.x) * 180 / Math.PI + 90

  const sourceOffsetX = (diffX * (sourceRadius + offsetOffset)) / pathLength
  const sourceOffsetY = (diffY * (sourceRadius + offsetOffset)) / pathLength
  const targetOffsetX = (diffX * (targetRadius + offsetOffset)) / pathLength
  const targetOffsetY = (diffY * (targetRadius + offsetOffset)) / pathLength

  const x1 = nodeSource.x + sourceOffsetX
  const y1 = nodeSource.y + sourceOffsetY
  const x2 = nodeTarget.x - targetOffsetX
  const y2 = nodeTarget.y - targetOffsetY

  return {
    source: {
      transform: `translate(${x1 - s}, ${y1 - s}) rotate(${angle + 180}, ${s}, ${s})`,
      d: `M 0,${s * 1.7} H ${s * 2} L ${s},0 Z`
    },
    target: {
      transform: `translate(${x2 - s}, ${y2 - s}) rotate(${angle}, ${s}, ${s})`,
      d: `M 0,${s * 1.7} H ${s * 2} L ${s},0 Z`
    }
  }
}

// Drag-and-drop nodes
function assignDragHandlers(elm, simulation, node, onDragEnd) {
  // Prepare d3 drag event handlers (https://bl.ocks.org/mbostock/4062045)
  const dragFunc = d3Drag()
    .on('start', (node) => {
      if (!d3Event.active) simulation.alphaTarget(0.3).restart()
      node.fx = node.x
      node.fy = node.y
    })
    .on('drag', (node) => {
      node.fx = d3Event.x
      node.fy = d3Event.y
    })
    .on('end', (node) => {
      if (!d3Event.active) simulation.alphaTarget(0).stop()
      node.fx = null
      node.fy = null
      const coordinates = {}
      simulation.nodes().forEach(node => coordinates[node.id] = { x: node.x, y: node.y })
      onDragEnd(coordinates)
    })
  // Use d3 selection to add handlers to element
  return elm && dragFunc(d3Select(elm).data([node]))
}

export function createSimulation(nodes, edges, width, height, size, linkForceDistance, nodeCollideRadius) {
  const { minNodeValue, maxNodeValue } = maxima(nodes, edges)
  const { minRadius, maxRadius } = NODE_RADIUS_RANGE[size]
  const nodeRadiusScale = areaRelativeRadiusScale(minNodeValue, maxNodeValue, minRadius, maxRadius)

  // Initialize forces
  const simulation = forceSimulation(nodes)
  const linkForce = forceLink(edges).id(d => d.id).distance(linkForceDistance)
  const nodeCollide = forceCollide(nodeCollideRadius)

  // Initialize force simulation
  simulation
    .force('charge_force', forceManyBody())
    .force('center_force', forceCenter(width / 2, height / 2))
    .force('box_force', boxForce(nodes, height, width, nodeRadiusScale))
    .force('links', linkForce)
    .force('collide', nodeCollide)
    //.on('end', this.handleSimulationEnd)

  return simulation
}

class GroupsNetworkGraph extends Component {
  constructor(props) {
    super(props)
    const hasCoordinates = props.nodeCoordinates && (Object.keys(props.nodeCoordinates).length > 0)
    let simulationEnded = false

    // Clone data (this is needed because d3-force is not pure)
    let nodes = JSON.parse(JSON.stringify(props.data.nodes))
    let edges = JSON.parse(JSON.stringify(props.data.edges))

    // Include initial node coordinates, if provided
    if (hasCoordinates) {
      nodes = nodes.map(node => ({
        ...node,
        ...props.nodeCoordinates[node.id]
      }))
    }

    // Prepare scales
    const { minNodeValue, maxNodeValue, minLinkValue, maxLinkValue } = maxima(nodes, edges)
    const { minRadius, maxRadius } = NODE_RADIUS_RANGE[props.size]
    const nodeRadiusScale = areaRelativeRadiusScale(minNodeValue, maxNodeValue, minRadius, maxRadius)
    const { minWidth, factor } = LINK_SIZE_FACTOR[props.size]
    const linkWidthScale = prepareLinkWidthScale(minLinkValue, maxLinkValue, minWidth, factor)
    const arrowWidthRange = ARROW_SIZE_RANGE[props.size]
    const arrowWidthScale = prepareArrowWidthScale(minLinkValue, maxLinkValue, arrowWidthRange)

    // Initialize simulation
    const { width, height, size, linkForceDistance, nodeCollideRadius } = props
    this.simulation = createSimulation(nodes, edges, width, height, size, linkForceDistance, nodeCollideRadius)
    this.simulation.on('end', this.handleSimulationEnd)
    //this.simulation = this.createSimulation(nodes, edges, nodeRadiusScale)

    // If coordinates provided, don't run simulation until triggered
    // (We still initialize it, in case user starts dragging, in which case it will get triggered)
    if (hasCoordinates) {
      this.simulation.stop()
      simulationEnded = true // Since we won't run simulation we can show graph on IE right away
    }

    // Prepare colors
    const ids = nodes.map(n => n.id)
    const { primaryColors } = makeGroupColorScales(props.theme, ids)

    // Store references to everything in component state
    this.state = {
      nodes: this.simulation.nodes(),
      hoveredEdge: null,
      hoveredNode: null,
      simulationEnded,
      edges,
      nodeRadiusScale,
      linkWidthScale,
      arrowWidthScale,
      primaryColors
    }

    this.bindSimulationTick()
  }

  componentDidMount() {
    this.updateSimulation(this.props)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.updateSimulation(nextProps)
  }

  componentWillUnmount() {
    this.unbindSimulationTick()
  }

  handleEdgeClick = (edge) => {
    const { onEdgeClick } = this.props
    if (onEdgeClick) {
      onEdgeClick(edge.source.id, edge.target.id, edge.isUniDirectional)
    }
  }

  handleNodeClick = (node) => {
    const { onNodeClick } = this.props
    if (onNodeClick) {
      onNodeClick(node.id)
    }
  }

  handleSimulationEnd = () => {
    let nodes = this.simulation.nodes()
    nodes = JSON.parse(JSON.stringify(nodes))
    this.props.onSimulationEnd(nodes)
    this.setState({ simulationEnded: true })
  }

  onSimulationTick() {
    this.frame = requestAnimationFrame(this.updatePositions.bind(this))
  }

  bindSimulationTick() {
    this.simulation.on('tick', this.updateSimulation.bind(this))
  }

  unbindSimulationTick() {
    this.simulation.on('tick', null)
    this.simulation.stop()
    this.frame = this.frame && cancelAnimationFrame(this.frame)
  }

  updateSimulation(props = this.props) {
    const { width, height, linkForceDistance, nodeCollideRadius } = props
    const { simulation } = this

    // Update center force, if needed
    const centerX = width / 2
    const centerY = height / 2
    if (simulation.force('center_force').x() !== centerX) {
      simulation.shouldRun = true
      simulation.force('center_force').x(centerX)
    }
    if (simulation.force('center_force').y() !== centerY) {
      simulation.shouldRun = true
      simulation.force('center_force').y(centerY)
    }

    // Update box force
    const boxForce = simulation.force('box_force')
    if (boxForce.height() !== height) {
      simulation.shouldRun = true
      boxForce.height(height)
    }
    if (boxForce.width() !== width) {
      simulation.shouldRun = true
      boxForce.width(width)
    }

    // Update link force
    if (simulation.force('links').distance()() !== linkForceDistance) {
      simulation.shouldRun = true
      simulation.force('links').distance(linkForceDistance)
    }

    // Update collision force
    if (simulation.force('collide').radius()() !== nodeCollideRadius) {
      simulation.shouldRun = true
      simulation.force('collide').radius(nodeCollideRadius)
    }

    if (simulation.shouldRun) {
      simulation.shouldRun = null
      simulation.alpha(1).restart()
    }

    this.onSimulationTick()
  }

  updatePositions() {
    this.setState({
      nodes: this.simulation.nodes()
    })
  }

  render() {
    const {
      theme,
      themes,
      width,
      height,
      edgeColors,
      inverted,
      tertiary,
      background,
      hasCurvedEdges,
      selfLinkAngle,
      showNodeTooltips,
      showEdgeTooltips,
      showGroupNames,
      draggable,
      onDragEnd,
      onEdgeClick,
      onNodeClick,
      compatibilityMode
    } = this.props
    const { nodes, edges, hoveredNode, nodeRadiusScale, linkWidthScale, arrowWidthScale } = this.state

    // Render nodes
    const nodeFill = determineNodeFill(this.props)
    const nodeStroke = determineNodeStroke(this.props)
    const nodeElements = nodes.map(node => {
      // Render filled circle
      const isHighlighted = (onNodeClick && hoveredNode && (hoveredNode.id === node.id))
      const style = {
        cursor: (draggable ? 'move' : (onNodeClick ? 'pointer' : 'default')),
        fill: nodeFill(node.id),
        stroke: nodeStroke(node.id),
        strokeWidth: '.5',
        opacity: (isHighlighted ? '0.5' : '1')
      }
      const refCallback = (elm => assignDragHandlers(elm, this.simulation, node, onDragEnd))
      const ref = (draggable ? refCallback : null)
      let radius = nodeRadiusScale(node.value)
      const onMouseOver = event => {
        this.setState({ hoveredNode: { ...node, target: event.target } })
      }
      const onMouseLeave = () => this.setState({ hoveredNode: null })
      const circle = (
        <circle
          key={node.id}
          ref={ref}
          style={style}
          r={radius}
          cx={node.x}
          cy={node.y}
          onMouseOver={onMouseOver}
          onMouseLeave={onMouseLeave}
          onClick={() => this.handleNodeClick(node)}
        />
      )

      // render node names if enabled
      if (showGroupNames) {
        radius = Math.max(radius, 10)
        const isLeftHalf = node.x < (width / 2)
        const isTopHalf = node.y < (height / 2)
        const GroupNames = props => (
          <text
            x={isLeftHalf ? node.x - radius : node.x + radius}
            y={isTopHalf ? node.y - radius : node.y + radius}
            textAnchor={isLeftHalf ? 'end' : 'start'}
            alignmentBaseline={isTopHalf ? 'baseline' : 'hanging'}
            fontFamily="Raleway"
            fontWeight="500"
            fontSize="13px"
            fill={theme.inverted.two[2]}
            {...props}
          >
            {node.name}
          </text>
        )

        return (
          <g key={node.id}>
            {circle}
            <GroupNames stroke={theme.inverted.two[0]} strokeWidth="5px" />
            <GroupNames />
          </g>
        )
      }

      return circle
    })

    const hatchPattern = (
      <pattern id="diagonalNodeHatch" width="4" height="4" patternTransform="rotate(135 0 0)" patternUnits="userSpaceOnUse">
        <line
          x1="0"
          y1="0"
          x2="0"
          y2="4"
          stroke={nodeStroke()}
          strokeWidth="2"
          strokeOpacity="0.5"
        />
      </pattern>
    )

    // Render links (paths)
    const linkColor = determineLinkColor(edgeColors, tertiary, inverted, theme)
    const arrowElements = edges.map((e, i) => {
      let startArrow, endArrow, line, lineHover
      const isSelfLink = (e.source.id === e.target.id)
      const rotation = isSelfLink ? `rotate(${selfLinkAngle}, ${e.source.x}, ${e.source.y})` : null
      const normalizedValue = linkWidthScale(e.value)
      const arrows = arrowHeads(e.source, e.target, e.value, arrowWidthScale, nodeRadiusScale)
      const fill = linkColor(e.themeId)
      // Arrows
      if (e.isUniDirectional && !isSelfLink) {
        startArrow = (
          <path
            className="arrow"
            transform={arrows.source.transform}
            d={arrows.source.d}
            fill={fill}
          />
        )
      }
      if (!isSelfLink) {
        endArrow = (
          <path
            className="arrow"
            transform={arrows.target.transform}
            d={arrows.target.d}
            fill={fill}
          />
        )
      }
      // Line
      line = (
        <path
          key={i}
          className='link'
          stroke={fill}
          colorRendering="optimizeQuality"
          strokeWidth={normalizedValue}
          d={linkPath(e.source, e.target, nodeRadiusScale, hasCurvedEdges, normalizedValue, e.isUniDirectional)}
          transform={rotation}
          fill="none"
        />
      )
      // Hover line (transparent edge with high width, makes it easier to hover for tooltips on thin edges)
      if (showEdgeTooltips) {
        const onMouseOver = event => {
          this.setState({ hoveredEdge: { ...e, target: event.target } })
        }
        const onMouseLeave = () => this.setState({ hoveredEdge: null })
        lineHover = (
          <path
            key={i}
            stroke="hotpink"
            strokeWidth="14"
            strokeOpacity="0"
            d={linkPath(e.source, e.target, nodeRadiusScale, hasCurvedEdges, normalizedValue, e.isUniDirectional)}
            transform={rotation}
            onMouseOver={onMouseOver}
            onMouseLeave={onMouseLeave}
            fill="none"
          />
        )
      }
      return (
        <EdgeGroup key={i} onClick={() => this.handleEdgeClick(e)} isClickable={onEdgeClick != null}>
          {endArrow}
          {startArrow}
          <g>
            {line}
          </g>
          <g>
            {lineHover}
          </g>
        </EdgeGroup>
      )
    })

    // Render edge tooltips, if enabled
    let tooltip
    if (showEdgeTooltips) {
      const { hoveredEdge } = this.state
      if (hoveredEdge) {
        tooltip = (
          <div>
            <Tooltip centered target={hoveredEdge.target}>
              <p>
                <FormattedMessage
                  id="GroupsNetworkGraph.edgeTooltip.header"
                  defaultMessage="{count} {count, plural, one {question} other {questions}}"
                  values={{ count: hoveredEdge.value }}
                />
              </p>
              <EdgeLegend inverted colors={edgeColors} edge={hoveredEdge} themes={themes} />
            </Tooltip>
          </div>
        )
      }
    }

    // Render node tooltip, if enabled
    if (showNodeTooltips) {
      const { hoveredNode } = this.state
      if (hoveredNode) {
        tooltip = (
          <div>
            <Tooltip target={hoveredNode.target}>
              <p>
                <strong>{hoveredNode.name}</strong>
              </p>
              <p>
                <FormattedMessage
                  id="GroupsNetworkGraph.nodeTooltip.content"
                  defaultMessage="received {count} {count, plural, one {question} other {questions}}"
                  values={{ count: hoveredNode.value }}
                />
              </p>
            </Tooltip>
          </div>
        )
      }
    }

    let bg
    if (background) {
      const bgColor = inverted ? theme.inverted.two[0] : theme.two[0]
      bg = (<rect width="100%" height="100%" fill={bgColor} />)
    }

    // Show spinner during simulation on IE11 (to avoid animation glitches)
    const isIE11 = compatibilityMode && !!window.MSInputMethodContext && !!document.documentMode
    if (!isIE11 || this.state.simulationEnded) {
      const svg = (
        <svg width={width} height={height}>
          {bg}
          {tertiary ? hatchPattern : null}
          <g>{arrowElements}</g>
          <g>{nodeElements}</g>
        </svg>
      )
      if (tooltip) {
        return (
          <div>
            {svg}
            {tooltip}
          </div>
        )
      }
      return svg
    } else {
      return (
        <div>Loading network visualization</div>
      )
    }
  }
}

GroupsNetworkGraph.createSimulation = createSimulation

GroupsNetworkGraph.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  linkForceDistance: PropTypes.number,
  nodeCollideRadius: PropTypes.number,
  selfLinkAngle: PropTypes.number,
  size: PropTypes.oneOf(['regular', 'small']),
  showNodeTooltips: PropTypes.bool,
  showEdgeTooltips: PropTypes.bool,
  showGroupNames: PropTypes.bool,
  draggable: PropTypes.bool,
  inverted: PropTypes.bool,
  secondary: PropTypes.bool,
  tertiary: PropTypes.bool,
  background: PropTypes.bool,
  onDragEnd: PropTypes.func,
  onSimulationEnd: PropTypes.func,
  edgeColors: PropTypes.func,
  onEdgeClick: PropTypes.func,
  nodeCoordinates: PropTypes.object, // Map of coordinates by id (id -> { x, y })
  compatibilityMode: PropTypes.bool
}

GroupsNetworkGraph.defaultProps = {
  width: 600,
  height: 600,
  linkForceDistance: -150,
  nodeCollideRadius: 60,
  selfLinkAngle: 55,
  size: 'regular',
  showNodeTooltips: true,
  showEdgeTooltips: false,
  showGroupNames: false,
  draggable: false,
  inverted: false,
  secondary: false,
  tertiary: false,
  background: false,
  onDragEnd: () => { },
  onSimulationEnd: () => { },
  nodeCoordinates: null,
  compatibilityMode: true // Do extra checks for IE support
}

export default withTheme(GroupsNetworkGraph)
