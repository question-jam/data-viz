import React from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'

const MAX_WIDTH = 300


const Bullet = ({ variant, color }) => {
  if (variant === 'themes') {
    return (<rect x={0} y={8} width={10} height={4} rx={2} ry={2}  fill={color} />)
  }
  return (<circle r={4} cx={4} cy={10} fill={color} />)
}

const Item = withTheme(({ text, variant, color, y, theme, inverted }) => (
  <g transform={`translate(0,${y})`}>
    <Bullet variant={variant} color={color} />
    <text
      x={16}
      y={14}
      fontFamily={theme.font1}
      fontWeight="500"
      fontSize={13}
      fill={inverted ? theme.inverted.one[0] : theme.one[0]}
    >
      {text}
    </text>
  </g>
))

const Legend = ({ variant, theme, groups, themes, colorScale, inverted, background }) => {
  let bg
  if (background) {
    const bgColor = inverted ? theme.inverted.two[0] : theme.two[0]
    bg = (<rect width="100%" height="100%" fill={bgColor} />)
  }

  let itemProps, height
  if(variant === 'themes') {
    height = (themes.length * 21 - 8)
    itemProps = themes.map((theme) => ({
      id: theme.themeId,
      text: theme.name
    }))
  } else {
    height = (groups.length * 21 - 8)
    itemProps = groups.map(group => ({
      id: group.groupId,
      text: group.name
    }))
  }

  return (
    <svg height={height} width={MAX_WIDTH}>
      {bg}
      {itemProps.map((item, i) => (
        <Item
          variant={variant}
          inverted={inverted}
          key={item.id}
          text={item.text}
          color={colorScale(item.id)}
          y={i * 21 - 4}
        />
      ))}
    </svg>
  )
}

Legend.propTypes = {
  variant: PropTypes.oneOf(['groups', 'themes']),
  inverted: PropTypes.bool,
  background: PropTypes.bool,
  groups: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    groupId: PropTypes.string.isRequired,
  })).isRequired,
  colorScale: PropTypes.func.isRequired
}

Legend.defaultProps = {
  variant: 'groups'
}

export default withTheme(Legend)
