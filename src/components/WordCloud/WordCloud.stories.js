import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import WordCloud from './WordCloud'
import * as d3 from 'd3'
import * as theme from '../theme'

import WORD_FREQS from './test_word_frequency'
import WORD_FREQS_MASTERCLASS from './word_frequency_masterclass'
import WORD_FREQS_SAAS_TOOLS from './word_frequency_saastools'
import WORD_FREQS_DEMO from './word_frequency_demo'

const wrapperStyle = {
  padding: 20
}

const colors = d3.scaleOrdinal(theme.yellow.data.primary).domain(d3.range(0, WORD_FREQS.length))

storiesOf('Data Visualization/WordCloud', module)
  .add('Case - AI Global', () => {
    return (
      <div style={wrapperStyle}>
        <WordCloud
          width={900}
          height={350}
          words={WORD_FREQS}
          colors={colors}
          onWordClick={action('Word clicked')}
          highlightedWords={['doctors', 'system']}
        />
      </div>
    )
  })
  .add('Case - Master class', () => {
    return (
      <div style={wrapperStyle}>
        <WordCloud
          width={900}
          height={350}
          words={WORD_FREQS_MASTERCLASS}
          colors={colors}
          onWordClick={action('Word clicked')}
        />
      </div>
    )
  })
  .add('Case - SaaS tools', () => {
    return (
      <div style={wrapperStyle}>
        <WordCloud
          width={900}
          height={350}
          words={WORD_FREQS_SAAS_TOOLS}
          colors={colors}
          onWordClick={action('Word clicked')}
        />
      </div>
    )
  })
  .add('Case - Demo', () => {
    return (
      <div style={wrapperStyle}>
        <WordCloud
          width={900}
          height={350}
          words={WORD_FREQS_DEMO}
          colors={colors}
          onWordClick={action('Word clicked')}
        />
      </div>
    )
  })
  .add('Small', () => {
    return (
      <div style={wrapperStyle}>
        <WordCloud
          width={285}
          height={180}
          words={WORD_FREQS}
          colors={colors}
        />
      </div>
    )
  })
  .add('Large', () => {
    return (
      <div style={wrapperStyle}>
        <WordCloud
          width={1500}
          height={800}
          words={WORD_FREQS}
          colors={colors}
        />
      </div>
    )
  })
  .add('With domain', () => {
    return (
      <div style={wrapperStyle}>
        <WordCloud
          width={900}
          height={350}
          words={WORD_FREQS.slice(0, 6)}
          colors={colors}
          domain={[7, 66]}
        />
      </div>
    )
  })
