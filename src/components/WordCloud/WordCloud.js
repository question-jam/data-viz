import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components'
import * as d3 from 'd3'
import cloud from 'd3-cloud'


const RootSvg = styled.svg`
  & text {
    cursor: ${({ isClickable }) => isClickable ? 'pointer' : 'default'};
  }
  & text:hover {
    ${({ theme, isClickable }) => isClickable ? `fill: ${theme.main.secondary};` : ''};
  }
`


const BASE_HEIGHT = 900
const BASE_FONT_MIN = 20
const BASE_FONT_MAX = 150

function determineColor(highlightedWords, theme, inverted) {
  const baseColor = inverted ? theme.inverted.one[0] : theme.one[0]
  return (word) => (highlightedWords.indexOf(word.text) !== -1) ? theme.main.primary : baseColor
}

export function renderCloud(width, height, wordFrequencies, theme, domain, canvas) {
  const list = wordFrequencies.map(r => ({ text: r.topWord, size: r.count }))

  // Establish domain based on prop or data itself
  if (!domain) {
    const maxSize = d3.max(list, d => d.size)
    const minSize = d3.min(list, d => d.size)
    domain = [minSize, maxSize]
  }

  // Make font scale to map data to predefined size (matching canvas)
  const fontScale = d3.scaleLinear()
    .domain(domain)
    .range([BASE_FONT_MIN, BASE_FONT_MAX])

  // Size canvas to match ratio of input width/height
  // (we wish to stick to a predefined ratio between fonts and canvas)
  const baseWidth = BASE_HEIGHT * (height / width)

  // Render cloud
  return new Promise(resolve => {
    const instance = cloud().size([BASE_HEIGHT, baseWidth])
      .words(list)
      .overflow(true)
      .padding(5)
      .rotate(0)
      .font(theme.font2)
      .fontSize(word => fontScale(word.size))
      .on('end', words => {
        // Return results merged onto corresponding wordFrequencies
        const merged = words.map((w, i) => ({ ...w, ...wordFrequencies[i] }))
        resolve(merged)
      })
    if(canvas) {
      instance.canvas(canvas)
    }
    instance.start()
  })
}

function hasCoordinates(words) {
  return words && (words.length > 0) && (words[0].x != null)
}

const WordCloud = props => {
  const { width, height, onWordClick, highlightedWords, theme, inverted, domain, canvas } = props
  const isClickable = (onWordClick != null)
  const color = determineColor(highlightedWords, theme, inverted)
  const [words, setWords] = useState(props.words);

  useEffect(() => {
    if ((words.length > 0) && !hasCoordinates(words)) {
      renderCloud(width, height, words, theme, domain, canvas)
        .then(renderedWords => setWords(renderedWords))
    }
  })

  if (hasCoordinates(words)) {
    // Prepare opacity scale
    const opacityMax = (BASE_FONT_MAX * 0.45) // Give full opacity to anything higher than 45% of max size
    const opacityScale = d3.scaleLinear()
      .domain([BASE_FONT_MIN, opacityMax])
      .range([0.4, 1.0])

    // Compute viewBox
    // Zoom render such that all words are visible, this is done by collecting coordinates
    // of words closest to the edge (i.e. left-most word dictates the left edge of the box)
    const minX = d3.min(words.map(w => w.x - (w.width / 2)))
    const maxX = d3.max(words.map(w => w.x + (w.width / 2)))
    const minY = d3.min(words.map(w => w.y - (w.height / 2)))
    const maxY = d3.max(words.map(w => w.y + (w.height / 2)))
    const widthBox = maxX - minX
    const heightBox = maxY - minY
    const viewBox = `${minX} ${minY} ${widthBox} ${heightBox}`

    // Render texts
    const texts = words.map((word, i) => (
      <text
        key={i}
        x={word.x}
        y={word.y}
        fontFamily={theme.font2}
        fontSize={word.size}
        fill={color(word)}
        fillOpacity={opacityScale(word.size)}
        textAnchor="middle"
        onClick={isClickable ? () => onWordClick(word.text) : null}
      >
        {word.text}
      </text>
    ))

    return (
      <RootSvg width={width} height={height} viewBox={viewBox} isClickable={isClickable}>
        {texts}
      </RootSvg>
    )
  }
  return null
}

WordCloud.renderCloud = renderCloud

WordCloud.propTypes = {
  words: PropTypes.arrayOf(
    PropTypes.shape({
      topWord: PropTypes.string.isRequired,
      count: PropTypes.number.isRequired,
      text: PropTypes.string,
      size: PropTypes.number,
      x: PropTypes.number,
      y: PropTypes.number,
      width: PropTypes.number,
      height: PropTypes.number
    })
  ).isRequired,
  domain: PropTypes.arrayOf(PropTypes.number),
  width: PropTypes.number,
  height: PropTypes.number,
  onWordClick: PropTypes.func,
  highlightedWords: PropTypes.arrayOf(PropTypes.string),
  inverted: PropTypes.bool
}

WordCloud.defaultProps = {
  highlightedWords: [],
  inverted: false
}

export default withTheme(WordCloud)
