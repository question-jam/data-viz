import React, { Component } from 'react'
import { withTheme } from 'styled-components'
import PropTypes from 'prop-types'

import RadialChart from './RadialChart'


const determineSize = (themes, width) => {
  let fontSize, radius
  const longestLabelLength = Math.max(...themes.map(t => t.name.length))
  if (longestLabelLength < 16) {
    fontSize = (width * 0.028)
    radius = (width * 0.22)
  } else if (longestLabelLength < 25) {
    fontSize = (width * 0.024)
    radius = (width * 0.18)
  } else {
    fontSize = (width * 0.02)
    radius = (width * 0.13)
  }
  return { fontSize, radius }
}

class QuestionThemesRadialChart extends Component {
  render() {
    const { themes, width, height, transparentBg, highlighted, theme: styledTheme } = this.props
    const { fontSize, radius } = determineSize(themes)

    let colors = this.props.colors
    if (highlighted) {
      colors = (id) => (highlighted.indexOf(id) !== -1) ? this.props.colors(id) : styledTheme.inverted.two[1]
    }

    const displayedData = themes.map(theme => ({
      label: theme.name,
      title: theme.name,
      value: theme.questionIds.length,
      fill: colors(theme.themeId),
    }))

    let bgColor
    if (!transparentBg) {
      bgColor = styledTheme.inverted.two[0]
    }

    return (
      <RadialChart
        inverted
        height={height}
        width={width}
        radius={radius}
        fontSize={fontSize}
        bgColor={bgColor}
        data={displayedData}
      />
    )
  }

  static propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    radius: PropTypes.number,
    transparentBg: PropTypes.bool,
    themes: PropTypes.arrayOf(PropTypes.shape({
      themeId: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      questionIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    })).isRequired,
    colors: PropTypes.func.isRequired
  }

  static defaultProps = {
    width: 660,
    height: 270,
    radius: 110,
    transparentBg: true
  }
}

export default withTheme(QuestionThemesRadialChart)
